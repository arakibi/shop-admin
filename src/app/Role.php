<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const ADMIN_ROLE = 'admin';
    const USER_MANEGER_ROLE = 'user_manager';
    const PRODUCT_MANAGER_ROLE = 'product_manager';

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
