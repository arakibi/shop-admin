<?php
declare(strict_types = 1);

namespace App\Repository;

use App\User;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Model;

class UsersRepository implements RepositoryInterface
{
    /**
     * @var User
     */
    private $model;

    /**
     * UsersRepository constructor.
     *
     * @param User $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * {@inheritDoc}
     */
    public function find(int $id): ?Model
    {
        return $this->model->find($id);
    }

    /**
     * {@inheritDoc}
     */
    public function save(array $attributes): ?Model
    {
        return $this->model->create($attributes);
    }

    /**
     * @param int $id
     *
     * @return Model|null
     */
    public function findWithRole(int $id): ?Model
    {
        return $this->model->with('role')->find($id);
    }

    /**
     * @param int $limit
     *
     * @return Paginator
     */
    public function paginate(int $limit = 10): Paginator
    {
        return $this->model->paginate($limit);
    }
}
