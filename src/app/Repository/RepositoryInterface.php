<?php
declare(strict_types = 1);

namespace App\Repository;

use Illuminate\Database\Eloquent\Model;

interface RepositoryInterface
{
    /**
     * Finds a model by its id.
     *
     * @param int $id
     *
     * @return Model|null
     */
    public function find(int $id): ?Model;

    /**
     * Saves the attributes for a model.
     *
     * @param array $attributes
     *
     * @return Model|null
     */
    public function save(array $attributes):? Model;
}
