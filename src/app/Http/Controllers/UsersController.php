<?php

namespace App\Http\Controllers;

use App\Http\Requests\UsersRequest;
use App\Repository\UsersRepository;
use App\Role;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Session;
use Psr\Log\LoggerInterface;

class UsersController extends Controller
{
    /**
     * @var UsersRepository
     */
    private $userRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * UsersController constructor.
     *
     * @param UsersRepository $userRepository
     * @param LoggerInterface $logger
     */
    public function __construct(UsersRepository $userRepository, LoggerInterface $logger)
    {
        $this->userRepository = $userRepository;
        $this->logger = $logger;
    }

    /**
     * Lists all the user.
     */
    public function index()
    {
        $users = $this->userRepository->paginate(3);

        return view('private.users.index', compact('users'));
    }

    /**
     * Returns the view for creating a new user.
     */
    public function create()
    {
        $roles = Role::pluck('name', 'id')->all();

        return view('private.users.create', compact('roles'));
    }

    /**
     * Shows one user.
     *
     * @param int $id
     */
    public function show(int $id)
    {
        $user = $this->userRepository->findWithRole($id);

        if ($user === null) {
            Session::flash('error', 'user could not be found for the given id: '.$id);

            return redirect()->route('users.index');
        }

        return view('private.users.show', compact('user'));
    }

    /**
     * @param UsersRequest $request
     *
     * @return RedirectResponse
     */
    public function store(UsersRequest $request): RedirectResponse
    {
        $attributes = $request->only('name', 'email', 'password', 'role_id');

        try {
            $this->userRepository->save($attributes);
        } catch (\Throwable $exception) {
            $this->logger->error('Could not save user', compact('attributes', 'exception'));
        }

        return redirect()->route('users.index');
    }
}
