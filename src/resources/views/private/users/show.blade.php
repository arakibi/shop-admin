@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Users</div>

                    <div class="card-body">

                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-4">
                                @include('layouts.private.sidebar')
                            </div>
                            <div class="col-8">
                                <table class="table">
                                    <theader>
                                        <tr>
                                            <td> Name</td>
                                            <td> Email</td>
                                            <td> Role</td>
                                            <td> Added on</td>
                                        </tr>
                                    </theader>
                                    <tbody>
                                        <tr>
                                            <td> {{ $user->name }}</td>
                                            <td> {{ $user->email }}</td>
                                            <td> {{ $user->role->name  }}</td>
                                            <td> {{ \Carbon\Carbon::parse($user->created_at)->diffForHumans()}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
