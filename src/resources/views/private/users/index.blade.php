@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Users</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-4">
                                @include('layouts.private.sidebar')
                            </div>
                            <div class="col-8">
                                <a href="{{ route('users.create') }}">
                                    <button class="btn btn-primary btn-lg"> Add a member</button>
                                </a>
                                <table class="table" style="margin-top: 15px;">
                                    <theader>
                                        <tr>
                                            <td> Name</td>
                                            <td> Email</td>
                                            <td> Role</td>
                                        </tr>
                                    </theader>
                                    <tbody>
                                        @foreach($users as $user)
                                            <tr>
                                                <td>
                                                    <a href="{{ route('users.show', $user->id) }}">
                                                        {{ $user->name }}
                                                    </a>
                                                </td>
                                                <td> {{ $user->email }}</td>
                                                <td> {{ $user->role->name  }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $users->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
