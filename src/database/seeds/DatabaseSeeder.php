<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = factory(Role::class )->create(['name' => Role::ADMIN_ROLE]);
        $productRole = factory(Role::class )->create(['name' => Role::PRODUCT_MANAGER_ROLE]);
        $userRole = factory(Role::class )->create(['name' => Role::USER_MANEGER_ROLE]);


        factory(User::class)->create([
            'role_id' => 1
        ]);

        factory(User::class, 2)->create([
            'role_id' => 2
        ]);

        factory(User::class, 2)->create([
            'role_id' => 3
        ]);
    }
}
