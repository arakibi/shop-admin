## Moroccan PHPers

This project is a demo of using laravel framework for building the backend of a shop.

Have question or need help? leave me a comment under the tutorial on youtube or join us on [facebook](https://www.facebook.com/groups/moroccanphpers).

## Set up the Project

### Set up the VM:
to set up the VM you need to run the command `vagrant up` from within the project folder.
 
* `cd src`
* `composer install`
* `npm install`
* `npm run dev`
* `php artisan migrate --seed`




